package dto;

public class passwordDto {
	
	//Aqu� creo las variables tipo private para configurar los constructores
	private int longitud;
	private String password;
	
	//Aqu� creo un constructor por defecto
	public passwordDto(){
	     this.longitud = 0;
	     this.password = "";
	 }
	  
	 
	 public passwordDto (int longitud){
	     this.longitud = longitud;
	     password = generarContrasenya(longitud);
	 }
	    
	//Aqu� creo un constructor donde password coge como valor un m�todo creado posteriormente
	public passwordDto(int longitud, String password) {
		super();
		this.longitud = longitud;
		this.password = generarContrasenya(longitud);
	}
	
	
	public String getPassword() {
		return password;
	}
	
	public String setPassword() {
		return null;
	}


	//Aqu� creo un m�todo que genera una contrase�a usando codigo ascii aleatorio
	private static String generarContrasenya(int longitud) {
		
		
		char[] array = new char[longitud];
		
		for (int i = 0; i < longitud; i++) {
			
			char numeroAleatorio = (char) (Math.random() * (122 - 48 + 1) + 48);
			array[i] = numeroAleatorio;
		}
		String charString = new String(array);

		return charString;
	}
	
	//Aqu� creo un m�todo, donde indicar� si la contrasenya generada por el anterior m�todo
	//�s segura o no
    public boolean contrasenaSegura(){
        int minusculasContador=0;
        int numerosContador=0;
        boolean seguridad = false;
        int mayusculasContador=0;
     
        //En este bucle tipo for analizamreos cada caracter de la contrase�a e dependiendo del tipo del caracter
        //se sumar� un contador o otro donde podr� hacer el c�luclo comprovando que sea una contrase�a segura
        for (int i=0;i<password.length();i++){
        	
             if (password.charAt(i) >= 97 && password.charAt(i) <= 122){
                	minusculasContador = minusculasContador + 1;
                	
             }else if(password.charAt(i) >= 65 && password.charAt(i) <= 90){
            	 mayusculasContador= mayusculasContador + 1;
            	 
             }else{
                	numerosContador= numerosContador + 1;
             }
            }
       
        //Si la contrase�a �s segura, devuelve true
        if (numerosContador >= 5 && mayusculasContador >= 2 && minusculasContador >= 1 ){
            return true;
        }
        //Si la contrase�a no �s segura, devuelve false
        else if (!(numerosContador >= 5) && !(mayusculasContador >= 2) && !(minusculasContador >= 1)){
            return false;
        }
        
        return seguridad;
    }
}