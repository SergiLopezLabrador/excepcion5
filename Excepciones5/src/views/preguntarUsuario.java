package views;
import dto.passwordDto;
import exceptions.exceptionUsuario;

public class preguntarUsuario {
	
	//En este m�todo ser� donde el usuario responder� las dos preguntas y se ejecutar�n los arrays 
	//y los m�todos de passwordDto
	public static void primeraPregunta() {
		
		//Aqu� pregunto al usuario por tamano de la array y de la longitud de la contrase�a
		int tamano = exceptionUsuario.tryCatch("De que tama�o quieres el array donde guardaremos las contrase�as");
		int longitud = exceptionUsuario.tryCatch("Que longitud quieres para la contrase�a");

		//Aqu� crear� dos arrays, una de ellas ser� para almancenar las contras�as generadas por el bucle for
		//y finalmente, el segundo array almacenar� booleans dependiendo si la contraseny almacernada en la primera array
		//�s considerada una contrase�a segura
		passwordDto[] arrayDeContrasenas = new passwordDto[tamano];
		boolean[] arrayDeSeguridad = new boolean[tamano];
		
		//En este bucle miramos si la contrasne�a del array �s segura o no, y la mostramos por consola a trav�s
		//de un System.out.println
	    for(int i=0;i<arrayDeContrasenas.length;i++){
	    	
	    	arrayDeContrasenas[i]=new passwordDto(longitud);
	        arrayDeSeguridad[i]=arrayDeContrasenas[i].contrasenaSegura();
	        	        
	        System.out.println(arrayDeContrasenas[i].getPassword()+" -> "+arrayDeSeguridad[i]);
	    }

	}	
}


