package exceptions;

import javax.swing.JOptionPane;

//Aqu� creo una m�todo donde recrea una pregunta muy b�sica que la reutilizar� para 
//hacer el try catch de todas las preguntas del programa
public class preguntaStandard {

	public static int preguntaStandard(String pregunta1) {
        int intStandard;
        String stringStandard = JOptionPane.showInputDialog(null, pregunta1);
        intStandard = Integer.parseInt(stringStandard);

        return intStandard;
    }
	
}
